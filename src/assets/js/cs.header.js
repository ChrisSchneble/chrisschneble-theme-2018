CS.header = {
    setupStickyHeader: function ($) {
        var header_admin;
        $(window).scroll(function () {
            if ($('#wpadminbar').length) {
                header_admin = $('#wpadminbar').height();
            } else {
                header_admin = 0;
            }

            if ($(document).scrollTop() > 0) {
                $('.cs-header').addClass('cs-sticky-header-active').css("top", header_admin);
                $('.cs-header-mobile').addClass('cs-sticky-header-active').css("top", header_admin);

            } else {
                $('.cs-header').removeClass('cs-sticky-header-active');
                $('.cs-header-mobile').removeClass('cs-sticky-header-active')
            }
        });
    },
    setupNavigation: function ($) {
        $("ul.cs-nav").superfish({
            delay: 800,
            hoverClass: 'cs-hover',
            animation: {opacity: "show", height: 'show'},
            animationOut: {opacity: "hide", height: 'hide'},
            easing: 'easeOutQuint',
            speed: 0.7,
            speedOut: 0,
            cssArrows: false,
            pathLevels: 2
        });
    },
    setupMobile: function ($) {
        var menu = $(".cs-header .cs-nav-dropdown").find(".c-hamburger");
        if (menu.hasClass("is-active") === true) {
            menu.removeClass("is-active");
        } else {
            menu.addClass("is-active");
        }

    },
    toggleMobile: function ($) {
        $('.cs-header-mobile .cs-nav-dropdown').click(function () {
            $('.cs-header-mobile-dropdown').stop(true, true).slideToggle(800, 'easeOutQuint');
            return false;
        });
    },
    hamburgerAnimation: function ($) {
        var toggles = document.querySelectorAll(".cs-header-mobile-navigation .c-hamburger");
        for (var i = toggles.length - 1; i >= 0; i--) {
            var toggle = toggles[i];
            toggleHandler(toggle);
        }
        function toggleHandler(toggle) {
            toggle.addEventListener("click", function (e) {
                e.preventDefault();
                (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
            });
        }
    },
    mobileFolders: function ($) {
        $('.cs-header-mobile-dropdown ul li:has(">ul") a').on('click', function () {
            $(this).parent().toggleClass('open');
            $(this).parent().find('> ul').stop(true, true).slideToggle(300, 'easeOutQuint');
            if ($(this).parent().hasClass('open')) {
                $(this).parent().find('ul ul').stop(true, true).slideUp(0, 'easeOutQuint');
            }

            if ($(this).parent().hasClass('menu-item-has-children')) {
                return false;
            }
        });
        $('.cs-header-mobile-dropdown').click(function (event) {
            event.stopPropagation();
        });
    }
};

jQuery(document).ready(function ($) {
    CS.header.setupStickyHeader($);
    CS.header.setupNavigation($);
    CS.header.setupMobile($);
    CS.header.hamburgerAnimation($);
    CS.header.toggleMobile($);
    CS.header.mobileFolders($)
});

