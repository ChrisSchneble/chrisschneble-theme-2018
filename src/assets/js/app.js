var CS = CS || {}; // make sure CS is available

jQuery(document).ready(function ($) {
    "use strict";


    /*** Page Loader ****/
    $(".cs-page-loader").fadeOut(100);
    $("body").css('overflow', 'visible');

    $(window).on('unload', function(){
        $('.cs-page-loader').fadeIn();
    });

    /*** Back To Top ***/
    if ($('.cs-back-to-top').length) {
        const scrollTrigger = 100, // px
            backToTop = function () {
                const scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('.cs-back-to-top').addClass('active');
                } else {
                    $('.cs-back-to-top').removeClass('active');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });

        $('.cs-back-to-top').on('click', function (e) {
            e.preventDefault();
            $(this).trigger('blur');
            $('html,body').animate({
                scrollTop: 0
            }, 500);
        });
    }

    $('.scroll-to-content').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: $("#start-main-content").offset().top
        }, 1000);
    });

    $("a[data-rel^=lightcase], a[rel^='cs-lightbox'], a[rel^='lightbox']").lightcase({
        maxWidth: 1200,
        maxHeight: 1200,
        overlayOpacity: 0.88,
        transition: 'elastic',
        showSequenceInfo: false,
        showCaption: false
    });

    /*** Animations ***/
    const wow = new WOW({
        boxClass: 'cs-animated',
        animateClass: 'animated',
        offset: 100,
        mobile: true,
        live: true,
        scrollContainer: null
    });
    wow.init();

    $('.cs-video-player .video-overlay').on("click", function (e) {
        $(this).hide();
        $(this).siblings('.video-headline').hide();
        const video = $(this).parent().find('iframe');
        video[0].src += "&autoplay=1";
        e.preventDefault();
    });
});
