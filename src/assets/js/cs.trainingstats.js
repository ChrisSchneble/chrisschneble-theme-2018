let stravaData = null;
jQuery(document).ready(function ($) {
    if ($('body').find('.trainingstats').length) {
        const toggle = $('.trainingStats-menu-bar .dropdown-menu li a');
        const checkChange = setInterval(() => {
            if (stravaData !== null) {
                console.log("Strava Data Fetch Complete");
                calculateAndPrintMonthlyStats(stravaData)
                clearInterval(checkChange);
            }
        }, 100);
        toggle.on("click", function (e) {
            e.preventDefault();
            toggle.toggle('active');
        });
        toggle.on("click", function () {
            const anchorText = $(this).text();
            $('.trainingStats-menu-bar .cs-button-text').text(anchorText);
            if ($(this).parent().hasClass("monthly")) {
                calculateAndPrintMonthlyStats(stravaData)
            }
            else {
                calculateAndPrintYearStats(stravaData);
            }
        });
    }

    function calculateAndPrintYearStats(stravaData) {
        let swim = parseFloat(stravaData.ytd_swim_totals.distance / 1000).toFixed(1)
        let bike = parseFloat(stravaData.ytd_ride_totals.distance / 1000).toFixed(1)
        let run = parseFloat(stravaData.ytd_run_totals.distance / 1000).toFixed(1)
        let total = parseFloat(swim) + parseFloat(bike) + parseFloat(run);
        printTrainingStats(swim, bike, run, total)
        animateCounter()
    }

    function calculateAndPrintMonthlyStats(stravaData) {
        let swim = parseFloat(stravaData.recent_swim_totals.distance / 1000).toFixed(1)
        let bike = parseFloat(stravaData.recent_ride_totals.distance / 1000).toFixed(1)
        let run = parseFloat(stravaData.recent_run_totals.distance / 1000).toFixed(1)
        let total = parseFloat(swim) + parseFloat(bike) + parseFloat(run);
        printTrainingStats(swim, bike, run, total)
        animateCounter()
    }

    function printTrainingStats(swim, bike, run , total) {
        $('.trainingstats .cs-counter-number.swim').text(swim);
        $('.trainingstats .cs-counter-number.bike').text(bike)
        $('.trainingstats .cs-counter-number.run').text(run)
        $('.trainingstats .cs-counter-number.total').text(total)

    }
    function animateCounter() {
        $('.cs-counter-number').each(function () {
            if (parseInt($(this).text(), 10) >= "10000") {
                $(this).addClass("bounce animated");
                $(this).css('font-size', '50px');
            }
            const $this = $(this);
            jQuery({Counter: 0.0}).animate({Counter: $this.text()}, {
                duration: 4000,
                easing: 'swing',
                step: function () {
                    if (isNaN(parseFloat(this.Counter))) {
                        $this.text("0.0");
                    } else {
                        $this.text(parseFloat(this.Counter).toFixed(1));
                    }
                }
            });
        });
    }
});

const auth_link = "https://www.strava.com/oauth/token"

function getActivites(res) {
    const activities_link = `https://www.strava.com/api/v3/athletes/12536828/stats?access_token=${res.access_token}`
    fetch(activities_link).then(response =>
        response.json().then(data => ({
                data: data,
            })
        ).then(res => {
            stravaData = res.data;
        }));
}

function reAuthorize(){
    fetch(auth_link,{
        method: 'post',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            client_id: '9352',
            client_secret: '67924beb2870e05c2c81992829df96389e26cae7',
            refresh_token: 'b23d3a7114e440fb61a4b8265b60928bd7d590e3',
            grant_type: 'refresh_token'
        })
    }).then(res => res.json()).then(res => getActivites(res))
}

reAuthorize()


function checkNaN(val) {
    if (isNaN(val)) {
        return 0;
    } else {
        return val;
    }
}

function getActualMonth() {
    let date = new Date();
    let month = [];
    month[0] = "JAN";
    month[1] = "FEB";
    month[2] = "MAR";
    month[3] = "APR";
    month[4] = "MAY";
    month[5] = "JUN";
    month[6] = "JUL";
    month[7] = "AUG";
    month[8] = "SEP";
    month[9] = "OCT";
    month[10] = "NOV";
    month[11] = "DEC";
    return month[date.getMonth()];
}





   

