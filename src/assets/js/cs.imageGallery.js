CS.imageGallery = {
    setupImageGallery: function ($) {
        $('.carousel').each(function () {
            let image_gallery_columns = parseInt($(this).attr('data-columns'));
            if (isNaN(image_gallery_columns)) {
                image_gallery_columns = 1;
            }
            let image_gallery_1024 = (image_gallery_columns >= 3) ? 3 : image_gallery_columns;
            let image_gallery_600 = (image_gallery_columns >= 2) ? 2 : image_gallery_columns;

            $(this).slick({
                infinite: true,
                dots: true,
                slidesToShow: image_gallery_columns,
                slidesToScroll: image_gallery_columns,
                autoplay: true,
                autoplaySpeed: 5000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: image_gallery_1024,
                            slidesToScroll: image_gallery_1024,
                        }
                    }, {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: image_gallery_600,
                            slidesToScroll: image_gallery_600
                        }
                    }, {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        });
    }

};


jQuery(document).ready(function ($) {
    CS.imageGallery.setupImageGallery($);
});

