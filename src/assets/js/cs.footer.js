CS.footer = {
    setupParallaxFooter: function ($) {
        $(window).on('load', function(){
            CS.footer.initializeFooterHeight($);
        });

        $(window).on( "resize", function() {
            clearTimeout(window.resizedFinishedFooter);
            window.resizedFinishedFooter = setTimeout(function () {
                CS.footer.initializeFooterHeight($);
            }, 500);
        });
    },
    footerVisible: function ($) {
        if ($(document).width() > 850) {
            if (( $(document).height() - ($(window).scrollTop() + $(window).height()) ) < $('.cs-footer').height()) {
                $('.cs-footer').css('opacity', '1');
            } else {
                $('.cs-footer').css('opacity', '0');
            }
        }
    },
    initializeFooterHeight: function ($) {
        if (!$('body').hasClass('cs-breadcrumb-active')) {
            if ($(document).width() > 850) {
                $("#wrapper > .content-container").css('margin-bottom', $('.cs-footer').height());
            } else {
                $("#wrapper > .content-container").css('margin-bottom', '');
            }
        }
    }
};


jQuery(document).ready(function ($) {
    CS.footer.setupParallaxFooter($);
    CS.footer.initializeFooterHeight($);
});

