<?php
/**
 * Header
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri() . '/favicon.ico';  ?>">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri() . '/src/images/Apple-Icon.png';  ?>">
    <?php wp_head(); ?>
    <link href="https://fonts.googleapis.com/css2?family=Plus+Jakarta+Sans:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">
</head>
<body <?php body_class(); ?>>


<div class="cs-page-loader cs-table">
    <div class="loader"></div>
</div>
<div id="page-container">

    <header class="primary-mobile primary-mobile-light">
        <?php get_template_part('templates/header-mobile'); ?>
    </header>
    <header class="primary-desktop primary-desktop-light">
        <?php get_template_part('templates/header'); ?>
    </header>


    <?php
    if (!is_front_page()) {
        get_template_part('templates/titlebar');
    }
    ?>

    <?php
    if (is_front_page()) {
        echo '<h1 class="hidden">Christian Schneble - Triathlet</h1>';
    }
    ?>

    <div id="wrapper">
        <div class="content-container cs-page-layout-full">
                    

