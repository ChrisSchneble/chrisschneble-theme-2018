(function () {
    'use strict';
}());

const sass = require('dart-sass');

module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        bower_concat: {
            all: {
                dest: {
                    'js': 'src/vendors/vendors.js',
                    'css': 'dist/css/vendors.css'
                },
                mainFiles: {
                    'bootstrap': [
                        'dist/css/bootstrap.css',
                        'dist/js/bootstrap.js'
                    ],
                    'jarallax': [
                        "dist/jarallax.js"
                    ],
                    "outlayer": [
                        "item.js",
                        "outlayer.js"
                    ]
                },
                exclude: [
                    'jquery'
                ]
            }
        },

        concat: {
            dist: {
                src: ['src/vendors/vendors.js', 'src/assets/js/*.js'],
                dest: 'dist/js/main.js'
            }
        },


        uglify: {
            options: {
                sourceMap: true,
                sourceMapName: 'dist/js/sourcemap.map'
            },
            dist: {
                files: {
                    'dist/js/main.min.js': ['dist/js/main.js']
                }
            }
        },

        sass: {
            dist: {
                options: { implementation: sass, sourceMap: true },
                files: {
                    'dist/css/styles.css': 'src/assets/scss/styles.scss'
                }
            }
        },

        cssmin: {
           dist: {
              files: {
                 'dist/css/vendors.min.css': ['dist/css/vendors.css']
              }
          }
        },


        watch: {
            options: {
                /*livereload: true*/
            },
            css: {
                files: ['src/assets/scss/*.scss'],
                tasks: ['sass:dist'],
                options: {
                    spawn: false,
                }
            },
            js: {
                files: ['src/assets/js/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    spawn: false,
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-bower-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.registerTask('default', ['bower_concat:all', 'concat', 'uglify', 'sass:dist', 'cssmin', 'watch']);
};
