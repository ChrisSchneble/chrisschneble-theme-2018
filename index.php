<?php
/**
 * The main template file
 *
 */

get_header(); ?>

<div class="container entry-content">
    <div id="content" class="content-with-sidebar-right">

        <?php if (have_posts()) : ?>
            <?php
            echo '<div class="cs-group blog-list" id="blog-list">';
            // Start the loop.
            while (have_posts()) : the_post();
                get_template_part('template-parts/content', 'teaser-list');
                // End the loop.
            endwhile;
            echo '</div>';

        cs_pagination();


// If no content, include the "No posts found" template.
        else :


        endif;
        ?>
    </div>
    <?php get_sidebar('sidebar-1'); ?>
    <?php /*if (!is_front_page() && is_home() && !is_single()) {

}
*/ ?>
</div>
<?php get_footer(); ?>
