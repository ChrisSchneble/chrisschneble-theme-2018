<?php
/**
 * Footer
 */
?>

    </div>
<!--</div>-->
<footer class="cs-footer">
    <div class="cs-footer-widgets">
        <div class="container">
            <div class="cs-footer-columns">
                <?php get_sidebar('content-bottom'); ?>
            </div>
        </div>
    </div>
    <div class="cs-copyrights">
        <div class="container container-padding">

            <div class="cs-copyrights-style2">
                <div class="cs-table-full">
                    <div class="cs-table-cell">
                        <div class="cs-copyrights-logo"><img src="<?php echo get_template_directory_uri() . '/src/images/logos/cs-logo-quer.svg'; ?>" class="cs-copyrights-image"
                                                             alt=""></div>
                    </div>
                    <div class="cs-table-cell">
                        <!-- <a href="<?php /*echo get_bloginfo('url') */ ?>/kontakt#impressum">Impressum</a> und <a href="<?php /*echo get_bloginfo('url') */ ?>/disclaimer">Disclaimer</a> | © 2017 cstheme</a>-->
                        <div class="cs-copyrights-text"><span class="developer-copyrights "> Copyright 2022 <a href="destinationstudios.christianschneble.de" target="blank"><strong>Destination Studios</strong>.</a> </span>
                            <span></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>


<div class="cs-back-to-top cs-back-to-top1">
    <i class="icon-arrow-up"></i>
</div>
</div>

<?php wp_footer(); ?>
<div id="blockingOverlay" class="blockingOverlay animated"></div>
</body>
</html>

