<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since cstheme 1.0
 */

get_header(); ?>

<main id="main" class="col-xs-12 top-100 bottom-80" role="main">

    <?php if (have_posts()) : ?>

        <header class="page-header">
            <h1 class="page-title text-l text-red"><?php printf(__('Suchergebnisse für: %s', 'cstheme'), '<span>' . esc_html(get_search_query()) . '</span>'); ?></h1>
        </header><!-- .page-header -->

        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            get_template_part('template-parts/content', '');

            // End the loop.
        endwhile;

        // Previous/next page navigation.
        the_posts_pagination(array(
            'prev_text' => __('Previous page', 'cstheme'),
            'next_text' => __('Next page', 'cstheme'),
            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('Page', 'cstheme') . ' </span>',
        ));

    // If no content, include the "No posts found" template.
    else :
        get_template_part('template-parts/content', 'none');

    endif;
    ?>
</main><!-- .site-main -->
<?php get_footer(); ?>
