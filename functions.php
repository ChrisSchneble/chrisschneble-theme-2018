<?php
/**
 * ChristianSchneble functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage ChrisSchneble
 * @since CS 1.0
 */


if (!function_exists('cstheme_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * Create your own cstheme_setup() function to override in a child theme.
     *
     * @since cstheme 1.0
     */
    function cstheme_setup()
    {

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for custom logo.
         *
         *  @since cstheme 1.2
         */
        add_theme_support('custom-logo', array(
            'height' => 240,
            'width' => 240,
            'flex-height' => true,
        ));

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1200, 9999);

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'cstheme'),
            //'social' => __('Social Links Menu', 'cstheme'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'status',
            'audio',
            'chat',
        ));

        /*
         * This theme styles the visual editor to resemble the theme style,
         * specifically font, colors, icons, and column width.
         */
        add_editor_style(array('css/editor-style.css', cstheme_fonts_url()));

        // Indicate widget sidebars can use selective refresh in the Customizer.
        add_theme_support('customize-selective-refresh-widgets');
    }
endif; // cstheme_setup
add_action('after_setup_theme', 'cstheme_setup');

/**
 * Registers a widget area.
 *
 */
function cstheme_widgets_init()
{
    register_sidebar(array(
        'name' => __('Sidebar', 'cstheme'),
        'id' => 'sidebar-1',
        'description' => __('Add widgets here to appear in your sidebar.', 'cstheme'),
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => __('Footer Bereich', 'cstheme'),
        'id' => 'sidebar-2',
        'description' => __('Erscheint unterhalb des Main Contents jeder Site', 'cstheme'),
        'before_widget' => '<div id="%1$s" class="widget-item %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}

add_action('widgets_init', 'cstheme_widgets_init');

if (!function_exists('cstheme_fonts_url')) :
    /**
     * Register Google fonts for cstheme.
     *
     * Create your own cstheme_fonts_url() function to override in a child theme.
     *
     * @return string Google fonts URL for the theme.
     * @since cstheme 1.0
     *
     */
    function cstheme_fonts_url()
    {
        $fonts_url = '';
        $fonts = array();
        $subsets = 'latin,latin-ext';

        /* translators: If there are characters in your language that are not supported by Raleway, translate this to 'off'. Do not translate into your own language. */
        /*if ('off' !== _x('on', 'Raleway font: on or off', 'cstheme')) {
            $fonts[] = 'Plus+Jakarta+Sans:200,300,400,500,600,700,800,900,400italic,700italic,';
        }*/
        /* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
        if ('off' !== _x('on', 'Montserrat font: on or off', 'serpentine')) {
            $fonts[] = 'Montserrat:400,700';
        }

        if ($fonts) {
            $fonts_url = add_query_arg(array(
                'family' => urlencode(implode('|', $fonts)),
                'subset' => urlencode($subsets),
            ), 'https://fonts.googleapis.com/css');
        }

        return $fonts_url;
    }
endif;


function excerpt($limit)
{
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

function content($limit)
{
    $content = explode(' ', get_the_content(), $limit);
    if (count($content) >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '...';
    } else {
        $content = implode(" ", $content);
    }
    $content = preg_replace('/\[.+\]/', '', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}


function the_title_limit($length, $replacer = '...')
{
    $string = the_title('', '', FALSE);
    if (strlen($string) > $length)
        $string = (preg_match('/^(.*)\W.*$/', substr($string, 0, $length + 1), $matches) ? $matches[1] : substr($string, 0, $length)) . $replacer;
    echo $string;
}


/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since cstheme 1.0
 */
/*function cstheme_javascript_detection()
{
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

add_action('wp_head', 'cstheme_javascript_detection', 0);*/

/**
 * Enqueues scripts and styles.
 *
 * @since cstheme 1.0
 */
function cstheme_scripts()
{
    // Add custom fonts, used in the main stylesheet.
    wp_enqueue_style('cstheme-fonts', cstheme_fonts_url(), array(), null);


    // Theme stylesheet.
    wp_enqueue_style('wp-style', get_stylesheet_uri());
    wp_enqueue_style('plugins', get_template_directory_uri() . '/dist/css/vendors.min.css', array(), '11092017');
    wp_enqueue_style('theme-styles', get_template_directory_uri() . '/dist/css/styles.css', array(), '11092017');

    //Theme Scripts
    wp_enqueue_script('cstheme-script', get_template_directory_uri() . '/dist/js/main.min.js', array('jquery'), '11092017', true);
}

add_action('wp_enqueue_scripts', 'cstheme_scripts');

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 * @since cstheme 1.0
 *
 */
function cstheme_body_classes($classes)
{

    if (!is_front_page()) {
        $classes[] = 'cs-body-header-sticky cs-footer-parallax cs-footer-paralalx-init cs-breadcrumb-active is-stretched fixed-nav landing';
    }

    if (is_front_page()) {
        $classes[] = 'cs-body-header-sticky cs-footer-parallax cs-footer-paralalx-init is-stretched fixed-nav';
    }


    return $classes;
}

add_filter('body_class', 'cstheme_body_classes');

/**
 * Converts a HEX value to RGB.
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 * @since cstheme 1.0
 *
 */
function cstheme_hex2rgb($color)
{
    $color = trim($color, '#');

    if (strlen($color) === 3) {
        $r = hexdec(substr($color, 0, 1) . substr($color, 0, 1));
        $g = hexdec(substr($color, 1, 1) . substr($color, 1, 1));
        $b = hexdec(substr($color, 2, 1) . substr($color, 2, 1));
    } else if (strlen($color) === 6) {
        $r = hexdec(substr($color, 0, 2));
        $g = hexdec(substr($color, 2, 2));
        $b = hexdec(substr($color, 4, 2));
    } else {
        return array();
    }

    return array('red' => $r, 'green' => $g, 'blue' => $b);
}

add_theme_support('post-thumbnails');


function wpdocs_theme_setup()
{
    add_image_size('teaser_thumbnail', 1200, 675, true);
    add_image_size('teaser_wide', 1150, 510, true);
    add_image_size('teaser_small', 864, 345, true);

}

add_action('after_setup_theme', 'wpdocs_theme_setup');

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 * @since cstheme 1.1
 *
 */
function cstheme_widget_tag_cloud_args($args)
{
    $args['largest'] = 1;
    $args['smallest'] = 1;
    $args['unit'] = 'em';
    return $args;
}

add_filter('widget_tag_cloud_args', 'cstheme_widget_tag_cloud_args');


/**
 * Disable responsive image support
 */

// Clean the up the image from wp_get_attachment_image()
add_filter('wp_get_attachment_image_attributes', function ($attr) {
    if (isset($attr['sizes']))
        unset($attr['sizes']);

    if (isset($attr['srcset']))
        unset($attr['srcset']);

    return $attr;

}, PHP_INT_MAX);

// Override the calculated image sizes
add_filter('wp_calculate_image_sizes', '__return_false', PHP_INT_MAX);

// Override the calculated image sources
add_filter('wp_calculate_image_srcset', '__return_false', PHP_INT_MAX);

// Remove the reponsive stuff from the content
remove_filter('the_content', 'wp_make_content_images_responsive');

function remove_img_attr($html)
{
    return preg_replace('/(width|height)="\d+"\s/', "", $html);
}

add_filter('post_thumbnail_html', 'remove_img_attr');


//removes jquery migrate
add_action('wp_default_scripts', function ($scripts) {
    if (!empty($scripts->registered['jquery'])) {
        $jquery_dependencies = $scripts->registered['jquery']->deps;
        $scripts->registered['jquery']->deps = array_diff($jquery_dependencies, array('jquery-migrate'));
    }
});

// remove emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');


function cs_panels_row_classes($classes, $grid)
{
    $classes[] = 'cs-section-container container';
    return $classes;
}

add_filter('siteorigin_panels_row_classes', 'cs_panels_row_classes', 10, 2);


function cs_widgets_collection($folders)
{
    $folders[] = get_template_directory() . '/widgets/';
    return $folders;
}

add_filter('siteorigin_widgets_widget_folders', 'cs_widgets_collection');

function cs_svg($svg_mime)
{
    $svg_mime['svg'] = 'image/svg+xml';
    return $svg_mime;
}

add_filter('upload_mimes', 'cs_svg');

function cs_pagination($pages = '', $range = 2)
{
    $showitems = ($range * 2) + 1;

    global $paged;
    if (empty($paged)) $paged = 1;

    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }

    if (1 != $pages) {
        echo "<div class='cs-pagination cs-default-color'><ul class='page-numbers'>";
        if ($paged > 2 && $paged > $range + 1 && $showitems < $pages) echo "<a href='" . get_pagenum_link(1) . "'>&laquo;</a>";
        if ($paged > 1 && $showitems < $pages) echo "<a href='" . get_pagenum_link($paged - 1) . "'>&lsaquo;</a>";

        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                echo ($paged == $i) ? "<li><span class='page-numbers current'>" . $i . "</span></li>" : "<li><a href='" . get_pagenum_link($i) . "' class='inactive' >" . $i . "</a></li>";
            }
        }

        if ($paged < $pages && $showitems < $pages) echo "<a href='" . get_pagenum_link($paged + 1) . "'>&rsaquo;</a>";
        if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages) echo "<a href='" . get_pagenum_link($pages) . "'>&raquo;</a>";
        echo "</ul></div>";
    }
}


function cs_readingTime()
{
    global $post;
    $myContent = $post->post_content;
    $words = str_word_count(strip_tags($myContent));
    $minutes = floor($words / 200);
    if ($minutes == 0) {
        $seconds = floor($words % 200 / (200 / 60));
        $est = $seconds . ' Sekunden';
    } else {
        $est = $minutes . ' Minuten';
    }

    return " | Lesedauer: " . $est;
}

function cs_readingTimeShort()
{
    global $post;
    $myContent = $post->post_content;
    $words = str_word_count(strip_tags($myContent));
    $minutes = floor($words / 200);
    if ($minutes == 0) {
        $seconds = floor($words % 200 / (200 / 60));
        $est = $seconds . ' Sekunden';
    } else {
        $est = $minutes . ' Minuten';
    }

    return "Lesedauer: " . $est;
}


/**
 * Show breadcrumbs tree
 */
if (!function_exists('jevelin_breadcrumbs')) {
    function cs_breadcrumbs()
    {

        // Set variable for adding separator markup
        $separator = '<span class="seperator"></span>';
        // Get global post object
        global $post;
        /***** Begin Markup *****/
        // Open the breadcrumbs
        $html = '<div class="breadcrumb">';
        // Add Homepage link & separator (always present)
        $html .= '<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="title" href="' . esc_url(home_url('/')) . '" title="Home"><span itempop="title">Home</span></a></span>';

        // Post

        if (is_singular('post')) {
            // Get post category info
            $category = get_the_category();
            // Get category values
            $category_values = array_values($category);
            // Get last category post is in
            $last_category = end($category_values);
            // Get parent categories
            $cat_parents = rtrim((string)get_category_parents($last_category->term_id, true, ','), ',');
            // Convert into array
            $cat_parents = explode(',', $cat_parents);
            // Loop through parent categories and add to breadcrumb trail

            $html .= $separator;
            foreach ($cat_parents as $parent) {
                $html .= '<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">' . wp_kses($parent, wp_kses_allowed_html('a')) . '</span>';
                $html .= $separator;
            }
            // add name of Post
            $html .= '<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="current-page" itemprop="url" href="#"><span itemprop="title">' . get_the_title() . '</span></a></span>';
        } // Page
        elseif (is_singular('page')) {
            // if page has a parent page
            if ($post->post_parent) {
                // Get all parents
                $parents = get_post_ancestors($post->ID);
                // Sort parents into the right order
                $parents = array_reverse($parents);
                // Add each parent to markup

                foreach ($parents as $parent) {
                    $html .= $separator;
                    $html .= '<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="bread-parent bread-parent-' . esc_attr($parent) . '" href="' . get_permalink($parent) . '" title="' . get_the_title($parent) . '">' . get_the_title($parent) . '</a></span>';

                }
            }
            // Current page
            $html .= $separator;
            $html .= '<span itemprop="title"><a class="current-page" itemprop="url" href="#"><span itemprop="title"> ' . get_the_title() . '</span></a></span>';
        } // Blog Overview
        elseif (is_home()) {
            $html .= $separator;
            $html .= '<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="current-page" itemprop="url" href="#"><span itemprop="title">Blog</span></a></span>';
        } // Attachment
        elseif (is_singular('attachment')) {
            // Get the parent post ID
            $parent_id = $post->post_parent;
            // Get the parent post title
            $parent_title = get_the_title($parent_id);
            // Get the parent post permalink
            $parent_permalink = get_permalink($parent_id);
            // Add markup
            $html .= 'attachment';
            $html .= '<span class="item-parent"><a class="bread-parent" href="' . esc_url($parent_permalink) . '" title="' . esc_attr($parent_title) . '">' . esc_attr($parent_title) . '</a></span>';
            $html .= $separator;
            // Add name of attachment
            $html .= '<span class="item-current item-' . $post->ID . '"><span title="' . get_the_title() . '"> ' . get_the_title() . '</span></span>';
        } // Custom Post Types
        elseif (is_singular()) {
            // Get the post type
            $post_type = get_post_type();
            // Get the post object
            $post_type_object = get_post_type_object($post_type);
            // Get the post type archive
            $post_type_archive = get_post_type_archive_link($post_type);

            $html .= "isSingular";
            // Add taxonomy link and separator
            $html .= '<span class="item-cat item-custom-post-type-' . esc_attr($post_type) . '"><a class="bread-cat bread-custom-post-type-' . esc_attr($post_type) . '" href="' . esc_url($post_type_archive) . '" title="' . esc_attr($post_type_object->labels->name) . '">' . esc_attr($post_type_object->labels->name) . '</a></span>';
            $html .= $separator;
            // Add name of Post
            $html .= '<span class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . $post->post_title . '">' . $post->post_title . '</span></span>';
        } // Category
        elseif (is_category()) {
            // Get category object
            $parent = get_queried_object()->category_parent;
            $html .= $separator;
            // If there is a parent category...
            if ($parent !== 0) {
                // Get the parent category object
                $parent_category = get_category($parent);
                // Get the link to the parent category
                $category_link = get_category_link($parent);
                // Output the markup for the parent category item
                $html .= '<span class="item-parent item-parent-' . esc_attr($parent_category->slug) . '"><a class="bread-parent bread-parent-' . esc_attr($parent_category->slug) . '" href="' . esc_url($category_link) . '" title="' . esc_attr($parent_category->name) . '">' . esc_attr($parent_category->name) . '</a></span>';
                $html .= $separator;
            }
            // Add category markup
            $html .= '<span itemprop="title"><a class="current-page" itemprop="url" href="#"><span itemprop="title">' . single_cat_title('', false) . '</span></a></span>';
        } // Tag
        elseif (is_tag()) {
            // Add tag markup
            $html .= $separator;
            $html .= '<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="current-page" itemprop="url" href="#"><span itemprop="title">' . single_tag_title('', false) . '</span></a></span>';
        } // Author
        elseif (is_author()) {
            // Add author markup
            $html .= "author";
            $html .= '<span class="item-current item-author"><span class="bread-current bread-author">' . get_queried_object()->display_name . '</span></span>';
        } // Day
        elseif (is_day()) {
            // Add day markup
            $html .= '<span class="item-current item-day"><span class="bread-current bread-day">' . get_the_date() . '</span></span>';
        } // Month
        elseif (is_month()) {
            // Add month markup
            $html .= '<span class="item-current item-month"><span class="bread-current bread-month">' . get_the_date('F Y') . '</span></span>';
        } // Year
        elseif (is_year()) {
            // Add year markup
            $html .= '<span class="item-current item-year"><span class="bread-current bread-year">' . get_the_date('Y') . '</span></span>';
        } // Custom Taxonomy
        elseif (is_archive()) {
            // get the name of the taxonomy
            $custom_tax_name = get_queried_object()->name;
            // Add markup for taxonomy
            $html .= "archive!";
            $html .= $separator;
            $html .= '<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="current-page" itemprop="url" href="#"><span itemprop="title">' . $custom_tax_name . '</span></a></span>';

        } // Search
        elseif (is_search()) {
            // Add search markup
            $html .= '<span class="item-current item-search"><span class="bread-current bread-search">' . sprintf(esc_html__('Search Results for "%s"', 'jevelin'), get_search_query()) . '</span></span>';
        }
        // Close breadcrumb container
        $html .= '</div>';
        apply_filters('ct_ignite_breadcrumbs_filter', $html);
        return wp_kses_post($html);
    }
}


// 1. Custom Post Type Registration (Events)

add_action('init', 'create_event_postype');

function create_event_postype()
{

    $labels = array(
        'name' => _x('Events', 'post type general name'),
        'singular_name' => _x('Event', 'post type singular name'),
        'add_new' => _x('Add New', 'events'),
        'add_new_item' => __('Add New Event'),
        'edit_item' => __('Edit Event'),
        'new_item' => __('New Event'),
        'view_item' => __('View Event'),
        'search_items' => __('Search Events'),
        'not_found' => __('No events found'),
        'not_found_in_trash' => __('No events found in Trash'),
        'parent_item_colon' => '',
    );

    $args = array(
        'label' => __('Events'),
        'labels' => $labels,
        'public' => true,
        'can_export' => true,
        'show_ui' => true,
        '_builtin' => false,
        '_edit_link' => 'post.php?post=%d', // ?
        'capability_type' => 'post',
        'menu_icon' => 'dashicons-clock',
        'hierarchical' => false,
        'rewrite' => array("slug" => "events"),
        'supports' => array('title', 'thumbnail', 'excerpt', 'editor'),
        'show_in_nav_menus' => true,
        'taxonomies' => array('tf_eventcategory', 'post_tag')
    );

    register_post_type('tf_events', $args);

}

// 2. Custom Taxonomy Registration (Event Types)
function create_eventcategory_taxonomy()
{

    $labels = array(
        'name' => _x('Categories', 'taxonomy general name'),
        'singular_name' => _x('Category', 'taxonomy singular name'),
        'search_items' => __('Search Categories'),
        'popular_items' => __('Popular Categories'),
        'all_items' => __('All Categories'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Edit Category'),
        'update_item' => __('Update Category'),
        'add_new_item' => __('Add New Category'),
        'new_item_name' => __('New Category Name'),
        'separate_items_with_commas' => __('Separate categories with commas'),
        'add_or_remove_items' => __('Add or remove categories'),
        'choose_from_most_used' => __('Choose from the most used categories'),
    );

    register_taxonomy('tf_eventcategory', 'tf_events', array(
        'label' => __('Event Category'),
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'event-category'),
    ));

}

add_action('init', 'create_eventcategory_taxonomy', 0);

// 3. Show Columns

add_filter("manage_edit-tf_events_columns", "tf_events_edit_columns");
add_action("manage_posts_custom_column", "tf_events_custom_columns");

function tf_events_edit_columns($columns)
{

    return array(
        "cb" => "<input type=\"checkbox\" />",
        "tf_col_ev_cat" => "Category",
        "tf_col_ev_date" => "Dates",
        "tf_col_ev_times" => "Times",
        "tf_col_ev_thumb" => "Thumbnail",
        "title" => "Event",
        "tf_col_ev_desc" => "Description",
    );

}

function tf_events_custom_columns($column)
{

    global $post;
    $custom = get_post_custom();
    switch ($column) {
        case "tf_col_ev_cat":
            // - show taxonomy terms -
            $eventcats = get_the_terms($post->ID, "tf_eventcategory");
            $eventcats_html = array();
            if ($eventcats) {
                foreach ($eventcats as $eventcat)
                    $eventcats_html[] = $eventcat->name;
                echo implode(", ", $eventcats_html);
            } else {
                _e('None', 'themeforce');
            }
            break;
        case "tf_col_ev_date":
            // - show dates -
            $startd = $custom["tf_events_startdate"][0];
            $endd = $custom["tf_events_enddate"][0];
            $startdate = date("F j, Y", $startd);
            $enddate = date("F j, Y", $endd);
            echo $startdate . '<br /><em>' . $enddate . '</em>';
            break;
        case "tf_col_ev_times":
            // - show times -
            $startt = $custom["tf_events_startdate"][0];
            $endt = $custom["tf_events_enddate"][0];
            $time_format = get_option('time_format');
            $starttime = date($time_format, $startt);
            $endtime = date($time_format, $endt);
            echo $starttime . ' - ' . $endtime;
            break;
        case "tf_col_ev_thumb":
            // - show thumb -
            $post_image_id = get_post_thumbnail_id(get_the_ID());
            if ($post_image_id) {
                $thumbnail = wp_get_attachment_image_src($post_image_id, 'post-thumbnail', false);
                if ($thumbnail) (string)$thumbnail = $thumbnail[0];
                echo '<img src="';
                echo bloginfo('template_url');
                echo '/timthumb/timthumb.php?src=';
                echo $thumbnail;
                echo '&h=60&w=60&zc=1" alt="" />';
            }
            break;
        case "tf_col_ev_desc";
            the_excerpt();
            break;

    }
}

// 4. Show Meta-Box

add_action('admin_init', 'tf_events_create');

function tf_events_create()
{
    add_meta_box('tf_events_meta', 'Events', 'tf_events_meta', 'tf_events');
}

function tf_events_meta()
{

    // - grab data -

    global $post;
    $custom = get_post_custom($post->ID);
    $meta_sd = $custom["tf_events_startdate"][0] ?? null;
    $meta_ed = $custom["tf_events_enddate"][0] ?? null;
    $meta_st = $meta_sd;
    $meta_et = $meta_ed;

    // - grab wp time format -
    $time_format = get_option('time_format');

    // - populate today if empty, 00:00 for time -

    if ($meta_sd == null) {
        $meta_sd = time();
        $meta_ed = $meta_sd;
        $meta_st = 0;
        $meta_et = 0;
    }

    // - convert to pretty formats -

    $clean_sd = date("d-m-Y", $meta_sd);
    $clean_ed = date("d-m-Y", $meta_ed);
    $clean_st = date($time_format, $meta_st);
    $clean_et = date($time_format, $meta_et);

    // - security -

    echo '<input type="hidden" name="tf-events-nonce" id="tf-events-nonce" value="' .
        wp_create_nonce('tf-events-nonce') . '" />';

    // - output -

    ?>
    <div class="tf-meta">
        <ul>
            <li><label>Start Datum</label><input name="tf_events_startdate" class="tfdate" value="<?php echo $clean_sd; ?>"/></li>
            <li><label>Start Zeit</label><input name="tf_events_starttime" value="<?php echo $clean_st; ?>"/></li>
            <li><label>End Datum</label><input name="tf_events_enddate" class="tfdate" value="<?php echo $clean_ed; ?>"/></li>
            <li><label>End Zeit</label><input name="tf_events_endtime" value="<?php echo $clean_et; ?>"/></li>
        </ul>
    </div>
    <?php
}

// 5. Save Data

add_action('save_post', 'save_tf_events');

function save_tf_events()
{

    global $post;

    // - still require nonce

    /*if (!wp_verify_nonce(isset($_POST['tf-events-nonce']), 'tf-events-nonce')) {
        return $post->ID;
    }

    if (!current_user_can('edit_post', $post->ID))
        return $post->ID;*/

    // - convert back to unix & update post
/*    if (!isset($_POST["tf_events_startdate"])):
        return $post;
    endif;*/

    $updatestartd = strtotime($_POST["tf_events_startdate"] . $_POST["tf_events_starttime"]);
    update_post_meta($post->ID, "tf_events_startdate", $updatestartd);

/*    if (!isset($_POST["tf_events_enddate"])):
        return $post;
    endif;*/

    $updateendd = strtotime($_POST["tf_events_enddate"] . $_POST["tf_events_endtime"]);
    update_post_meta($post->ID, "tf_events_enddate", $updateendd);

}

// 6. Customize Update Messages

add_filter('post_updated_messages', 'events_updated_messages');

function events_updated_messages($messages)
{

    global $post, $post_ID;

    $messages['tf_events'] = array(
        0 => '', // Unused. Messages start at index 1.
        1 => sprintf(__('Event updated. <a href="%s">View item</a>'), esc_url(get_permalink($post_ID))),
        2 => __('Custom field updated.'),
        3 => __('Custom field deleted.'),
        4 => __('Event updated.'),
        /* translators: %s: date and time of the revision */
        5 => isset($_GET['revision']) ? sprintf(__('Event restored to revision from %s'), wp_post_revision_title((int)$_GET['revision'], false)) : false,
        6 => sprintf(__('Event published. <a href="%s">View event</a>'), esc_url(get_permalink($post_ID))),
        7 => __('Event saved.'),
        8 => sprintf(__('Event submitted. <a target="_blank" href="%s">Preview event</a>'), esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
        9 => sprintf(__('Event scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview event</a>'),
            // translators: Publish box date format, see http://php.net/date
            date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)), esc_url(get_permalink($post_ID))),
        10 => sprintf(__('Event draft updated. <a target="_blank" href="%s">Preview event</a>'), esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
    );

    return $messages;
}

// 7. JS Datepicker UI

function events_styles()
{
    global $post_type;
    if ('tf_events' != $post_type)
        return;
    wp_enqueue_style('ui-datepicker', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css');
    // Enqueue the datepicker CSS
    wp_enqueue_style('my-jquery-ui-datepicker', get_bloginfo('template_url') . '/backendExtension/css/theme.css', false, false, false);
}

function events_scripts()
{
    global $post_type;
    if ('tf_events' != $post_type)
        return;
    wp_enqueue_script('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', array('jquery'));
    wp_enqueue_script('ui-datepicker', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js');
    wp_enqueue_script('custom_script', get_bloginfo('template_url') . '/backendExtension/js/pubforce-admin.js', array('jquery'));
}


// Extend archive Titel
add_action('admin_print_styles-post.php', 'events_styles', 1000);
add_action('admin_print_styles-post-new.php', 'events_styles', 1000);

add_action('admin_print_scripts-post.php', 'events_scripts', 1000);
add_action('admin_print_scripts-post-new.php', 'events_scripts', 1000);

add_filter('get_the_archive_title', function ($title) {
    if (is_category()) {
        $title = single_cat_title('', false);
    } elseif (is_tag()) {
        $title = single_tag_title('', false);
    }
    return $title;
});

//Infinite Scroll
function wp_infinitepaginate()
{
    $loopFile = $_POST['loop_file'];
    $paged = $_POST['page_no'];
    $action = $_POST['what'];
    $value = $_POST['value'];

    if ($action == 'author_name') {
        $arg = array('author_name' => $value, 'paged' => $paged, 'post_status' => 'publish');
    } elseif ($action == 'category_name') {
        $arg = array('category_name' => $value, 'paged' => $paged, 'post_status' => 'publish');
    } elseif ($action == 'search') {
        $arg = array('s' => $value, 'paged' => $paged, 'post_status' => 'publish');
    } else {
        $arg = array('paged' => $paged, 'post_status' => 'publish');
    }
    # Load the posts
    query_posts($arg);
    get_template_part($loopFile);

    exit;
}

add_action('wp_ajax_infinite_scroll', 'wp_infinitepaginate'); // for logged in user
add_action('wp_ajax_nopriv_infinite_scroll', 'wp_infinitepaginate'); // if user not logged in

//Teaser Fallback Image
function get_attachment_id_from_src($image_src)
{
    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
    $id = $wpdb->get_var($query);
    return $id;
}

add_filter('post_thumbnail_html', 'my_post_thumbnail_html');

function my_post_thumbnail_html($html)
{

    if (empty($html))
        $html = '<img src="' . trailingslashit(get_stylesheet_directory_uri()) . 'src/images/fallbackImage.png' . '" alt="No Teaser Image available" />';

    return $html;
}

function max_title_length($title)
{
    $max = 48;
    if (strlen($title) > $max) {
        return substr($title, 0, $max) . " &hellip;";
    } else {
        return $title;
    }
}

add_filter('the_title', 'max_title_length');
