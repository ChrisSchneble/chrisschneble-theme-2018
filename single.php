<?php
/**
 * The template for displaying all single posts
 *
 */

get_header(); ?>

<div class="container entry-content">
    <div id="content">


        <div class="blog-single">
            <?php
            // Start the loop.
            while (have_posts()) : the_post();

                // Include the single post content template.
                get_template_part('template-parts/content', 'single');


            endwhile;
            ?>

        </div>
    </div>
</div>
<?php get_footer(); ?>
