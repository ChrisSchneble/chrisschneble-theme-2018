<?php
/**
 * The sidebar containing the main widget area.
 */

global $post;
?>

<div id="sidebar" class="sidebar-right">
    <?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #sidebar -->
