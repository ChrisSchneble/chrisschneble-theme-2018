<div id="header-mobile" class="cs-header-mobile cs-sticky-mobile-header">
    <div class="cs-header-mobile-navigation">
        <div class="container">
            <div class="cs-table">
                <div class="cs-table-cell cs-group">
                    <div class="header-logo cs-group-equal">
                        <a href="<?php echo get_bloginfo('url') ?>" class="header-logo-container cs-table-small">
                            <div class="cs-table-cell">
                                <img class="cs-standard-logo" src="<?php echo get_template_directory_uri() . '/src/images/logos/cs-logo-quer-weiss.svg'; ?>" alt="Christian Schneble Logo">
                                <img class="cs-sticky-logo" src="<?php echo get_template_directory_uri() . '/src/images/logos/cs-logo-quer.svg'; ?>" alt="Christian Schneble Logo">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="cs-table-cell">
                    <nav id="header-navigation-mobile" class="header-standard-position">
                        <div class="cs-nav-container">
                            <ul class="cs-nav">
                                <li class="menu-item cs-nav-dropdown">
                                    <a href="#">
                                        <div class="cs-table-full">
                                            <div class="cs-table-cell">
                                                <span class="c-hamburger c-hamburger--htx">
                                                    <span>Toggle menu</span>
                                                </span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <nav class="cs-header-mobile-dropdown">
        <div class="container cs-nav-container">
            <?php
            $menu_args = array('theme_location' => 'primary', 'menu_id' => 'menu-mobile',  'menu_class' => 'cs-nav-mobile', 'container' => false);
            $hide_top_nav = '';
            wp_nav_menu($menu_args);
            ?>
        </div>
    </nav>
</div>
