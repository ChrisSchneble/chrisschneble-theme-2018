
<div class="cs-header-height">
    <div class="cs-header cs-header-1 cs-sticky-header cs-header-small-icons">
        <div class="container">
            <div class="cs-table">
                <div class="cs-table-cell cs-group">
                    <div class="header-logo cs-group-equal">
                        <a href="<?php echo get_bloginfo('url') ?>" class="header-logo-container cs-table-small">
                            <div class="cs-table-cell">
                                <img class="cs-standard-logo" src="<?php echo get_template_directory_uri() . '/src/images/logos/cs-logo-quer-weiss.svg'; ?>" alt="Christian Schneble Logo">
                                <img class="cs-sticky-logo" src="<?php echo get_template_directory_uri() . '/src/images/logos/cs-logo-quer.svg'; ?>" alt="Christian Schneble Logo">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="cs-table-cell">
                    <nav id="header-navigation" class="header-standard-position">
                        <?php
                            $menu_args = array('theme_location' => 'primary', 'menu_class' => 'cs-nav', 'container_class' => 'cs-nav-container');
                            $hide_top_nav = '';
                            wp_nav_menu($menu_args);
                            ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
