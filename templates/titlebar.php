<?php
/**
 * Titlebar HTML
 */

?>
<?php $style = ''?>
<?php if (has_post_thumbnail() && is_singular( 'post' )):
    $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'teaser_wide');
    $style = 'style="background-image: url(' . $featured_img_url . ');"';
    ?>
<?php endif; ?>


<div class="cs-titlebar cs-titlebar-center" <?php echo $style ?>>
    <div class="container">
        <div class="cs-table cs-titlebar-height-medium">
            <div class="cs-table-cell">
                <div class="titlebar-title">
                    <h1 class="titlebar-title-h1">
                        <?php
                        if (is_archive()) :
                            echo get_the_archive_title();
                        elseif( is_404() ) :
                        	echo '404 Page not found';
                        elseif( is_singular( 'post' )) :
                        	echo single_post_title();
                        else :
                            echo single_post_title();
                        endif;
                        ?>
                    </h1>
                    <div class="title-level">
                        <?php echo cs_breadcrumbs() ?>
                    </div>
                    <?php if (is_singular( 'post' )) :
                        echo '<div class="post-meta-date fadeIn animated">' . get_the_date() . '<span class="cs-readingtime">'. cs_readingTime() . '</span></div>';
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>




