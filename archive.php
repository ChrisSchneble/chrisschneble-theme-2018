<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since cstheme 1.0
 */

get_header(); ?>

<div class="container entry-content">
    <div id="content" class="content-with-sidebar-right">

    <?php if (have_posts()) : ?>
        <?php
        echo '<div class="cs-group blog-list" id="blog-list">';
        // Start the loop.
        while (have_posts()) : the_post();
            get_template_part('template-parts/content', 'teaser-list');
            // End the loop.
        endwhile;
        echo '</div>';

    cs_pagination();





// If no content, include the "No posts found" template.
else :
    get_template_part('template-parts/content', 'none');

endif;
?>
    </div>
    <?php get_sidebar('sidebar-1');  ?>
<?php /*if (!is_front_page() && is_home() && !is_single()) {

}
*/?>
</div>
<?php get_footer(); ?>


