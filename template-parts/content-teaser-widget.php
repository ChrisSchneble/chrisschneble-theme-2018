<div class="cs-recent-posts-widgets-item">
    <div class="cs-recent-posts-widgets-item-thumb">
        <a href="<?php the_permalink(); ?>">
            <div class="cs-ratio">
                <div class="cs-ratio-container cs-ratio-container-square">
                    <div class="cs-ratio-content" style="background-image: url(<?php the_post_thumbnail_url('small'); ?>);"></div>
                </div>
            </div>
            <div class="cs-mini-overlay">
                <div class="cs-mini-overlay-container">
                    <div class="cs-table-full">
                        <div class="cs-table-cell">
                            <i class="icon-link"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="cs-recent-posts-widgets-item-content">
        <a href="<?php the_permalink(); ?>">
            <?php the_title('<h4>', '</h4>'); ?>
        </a>
        <div class="cs-recent-posts-widgets-item-meta">
            <span class="post-meta-date">
                <span><?php echo get_the_date(); ?></span>
            </span>
        </div>

    </div>
</div>
