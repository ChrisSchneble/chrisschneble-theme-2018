<?php
$category = get_the_category();
$name = $category[0]->cat_name;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('teaser-standard stage post-item'); ?>>
        <div class="cs-background-overlay">
            <?php the_post_thumbnail('full', ['loading' => false ]);  ?>
        </div>
        <div class="container">
            <div class="post-content-container">
                <div class="slug-wrapper">
                    <span class="cs-category-slug fadeIn animated"><?php echo $name ?></span>
                </div>
                <a href="<?php the_permalink(); ?>" class="post-title">
                    <?php the_title('<h2>', '</h2>'); ?>
                </a>
                <div class="post-content">
                    <?php echo excerpt(28);  ?>
                </div>
                <div class="post-meta">
                    <div class="post-meta-date">
                        <span><i class="fa fa-calendar-o"></i><?php echo get_the_date(); ?></span>
                    </div>
                    <div class="cs-meta-reading-time">
                        <span><i class="fa fa-clock-o"></i><?php echo cs_readingTimeShort(); ?></span>
                    </div>
                </div>
        </div>
        <a href="<?php the_permalink(); ?>" class="cs-overlay-link"></a>
</article>
