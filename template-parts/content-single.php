<?php
/**
 * The template part for displaying single posts
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('main-post post-item');  ?>>
    <div class="post-container">
        <div class="post-content">
            <?php the_content();  ?>
        </div>
    </div>
</article>
<div class="cs-clear"></div>
<?php
$posttags = get_the_tags();
if ($posttags) {
    echo '<div class="cs-blog-tags"><h5>Tags</h5><div class="cs-blog-tags-list">';
    foreach ($posttags as $tag) {
        echo '<a href="' . get_tag_link($tag->term_id) . '" rel="tag" class="cs-blog-tag-item">' . $tag->name . '</a>';
    }
    echo '</div></div>';
}
?>

<!--<div class="cs-blog-single-meta row">
    <div class="col-md-6 col-sm-6 col-xs-6">
        <div class="cs-blog-social">
            <div class="cs-social-share">
                <div class="cs-social-share-button cs-noselect"><i class="icon-share"></i><span>Teilen</span></div>
                <div class="cs-social-share-networks"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="cs-page-switcher">
            <?php /*if (get_previous_post() != null) : */?>
                <?php
/*                # get_adjacent_post( $in_same_cat = false, $excluded_categories = '', $previous = true )
                $next_post_obj  = get_adjacent_post( '', '', false );
                $next_post_ID   = isset( $next_post_obj->ID ) ? $next_post_obj->ID : '';
                $next_post_link     = get_permalink( $next_post_ID );
                $prev_post_obj  = get_adjacent_post( '', '', true );
                $prev_post_ID   = isset( $prev_post_obj->ID ) ? $prev_post_obj->ID : '';
                $prev_post_link     = get_permalink( $prev_post_ID );
                */?>
                <a href="<?php /*echo $prev_post_link; */?>" rel="next" class="cs-page-switcher-button">
                    <i class="ti-arrow-left"></i>
                </a>
            <?php /*endif; */?>
            <?php
/*            $query = new WP_Query( array(
                'posts_per_page' => -1,
                'post_status' => 'publish',
                'orderby' => 'date', // be sure posts are ordered by date
                'order' => 'ASC', // be sure order is ascending
                'fields' => 'ids' // get only post ids
            ));
            global $post; // current post object
            $i = array_search( $post->ID, $query->posts ) + 1; // add 1 because array are 0-based
            echo "<span class=\"cs-page-switcher-content\"><strong>{$i}</strong> / {$query->post_count}</span>";
            */?>
            <?php /*if (get_next_post() != null) : */?>
                <a href="<?php /*echo $next_post_link; */?>" rel="next" class="cs-page-switcher-button">
                    <i class="ti-arrow-right"></i>
                </a>
            <?php /*endif; */?>
        </div>
    </div>
</div>-->


<?php
//for use in the loop, list  post titles related to first tag on current post
$tags = wp_get_post_tags($post->ID);
if ($tags) {
    $first_tag = $tags[0]->term_id;
    $args = array(
        'tag__in' => array($first_tag),
        'post__not_in' => array($post->ID),
        'posts_per_page' => 3,
    );
    $my_query = new WP_Query($args);
    if ($my_query->have_posts()) {
        echo '<div class="cs-related-posts-wrapper"><h5>Ähnliche Artikel</h5><div class="cs-related-posts">';
        while ($my_query->have_posts()) : $my_query->the_post(); ?>
            <?php echo '<div class="cs-related-posts--item">';?>
            <?php get_template_part('template-parts/content-teaser-background_Image'); ?>
            <?php echo '</div>';?>
            <?php
        endwhile;
        echo '</div></div>';
    }
    wp_reset_query();
}
?>

