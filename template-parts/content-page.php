<?php
/**
 * /**
 * The template part for displaying page contents
 *
 */
?>

<?php
if (is_page("kontakt") || is_page('termine')) {
    $sidebar = '';
} else {
    $sidebar = "content-with-sidebar-right";
}
?>


<div class="container entry-content">

    <div id="content" class="page-content <?php echo $sidebar ?> the-page">
        <?php
        while (have_posts()) : the_post();
            the_content();
        endwhile;
        ?>
    </div>
    <?php if (!(is_page("kontakt") || is_page('termine'))) {
        get_sidebar('sidebar-1');
    }
    ?>
</div>


