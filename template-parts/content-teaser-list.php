<?php
$post_id = get_the_ID();
$post_title = get_the_title();
$post_url = get_permalink();
$category = get_the_category();
$name = $category[0]->cat_name;
$cat_id = get_cat_ID($name);
$link = get_category_link($cat_id);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('teaser-standard post-item list'); ?>>
    <div class="post-container">
        <div class="post-meta-thumb">
            <?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('teaser_thumbnail'); ?>
            <?php else : ?>
                <img src="<?php echo get_template_directory_uri(); ?>/images/fallbackImage.jpg" alt="Fallback Image"/>
            <?php endif; ?>

            <div class="cs-overlay-style1">
                <div class="cs-table-full">
                    <a href="<?php the_permalink(); ?>" class="cs-overlay-item cs-table-cell">
                        <div class="cs-overlay-item-container">
                            <i class="icon-link"></i>
                        </div>
                    </a>
                </div>
            </div>

        </div>
        <div class="post-content-container">
            <div class="slug-wrapper">
                <span class="cs-category-slug fadeIn animated"><?php echo $name ?></span>
            </div>
            <a href="<?php the_permalink(); ?>" class="post-title">
                <?php the_title('<h2>', '</h2>'); ?>
            </a>
            <div class="post-content">
                <?php echo excerpt(22); ?>
            </div>
            <div class="post-meta">
                <div class="post-meta-date">
                    <span><i class="fa fa-calendar-o"></i><?php echo get_the_date(); ?></span>
                </div>
                <div class="cs-meta-reading-time">
                    <span><i class="fa fa-clock-o"></i><?php echo cs_readingTimeShort(); ?></span>
                </div>
            </div>
        </div>
    </div>
</article>

