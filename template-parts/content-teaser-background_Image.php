<article id="post-<?php the_ID(); ?>" <?php post_class('teaser-background-image post-item'); ?>>
    <a href="<?php the_permalink(); ?>">
        <div class="post-container">
            <div class="post-content-container">
                <?php the_title('<h2>', '</h2>'); ?>
                <div class="post-content">
                    <?php echo excerpt(14);  ?>
                </div>
                <div class="post-meta">
                    <div class="post-meta-date">
                        <span><i class="fa fa-calendar-o"></i><?php echo get_the_date(); ?></span>
                    </div>
                    <div class="cs-meta-reading-time">
                        <span><i class="fa fa-clock-o"></i><?php echo cs_readingTimeShort(); ?></span>
                    </div>
                </div>
            </div>
            <span class="img-link">
                <?php the_post_thumbnail('teaser_thumbnail');  ?>
            </span>
        </div>
    </a>
</article>


