<?php
$post_id = get_the_ID();
$post_title = get_the_title();
$post_url = get_permalink();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('teaser-standard event post-item'); ?>>
    <div class="post-container">
        <div class="post-meta-thumb">
            <?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('teaser_thumbnail'); ?>
            <?php else : ?>
                <img src="<?php echo get_template_directory_uri(); ?>/images/fallbackImage.jpg" alt="Fallback Image"/>
            <?php endif; ?>

            <div class="cs-overlay-style1">
                <div class="cs-table-full">
                    <a href="<?php echo get_page_link(get_page_by_title( 'termine' )->ID )?>" class="cs-overlay-item cs-table-cell">
                        <div class="cs-overlay-item-container">
                            <i class="icon-link"></i>
                        </div>
                    </a>
                </div>
            </div>

        </div>
        <div class="post-content-container">
            <?php
            $custom = get_post_custom(get_the_ID());
            $start_date = $custom["tf_events_startdate"][0];
            $end_date = $custom["tf_events_enddate"][0];
            $time_format = get_option('time_format');
            $start_time = date($time_format, $start_date);
            $end_time = date($time_format, $end_date);

            if ($start_date) {
                $start_date = date("d-m-Y", $start_date);
            }

            if ($end_date) {
                $end_date = date("d-m-Y", $end_date);
            }
            setlocale(LC_TIME, 'german', 'deu_deu', 'deu', 'de_DE', 'de');
            $start_date = strtotime($start_date);
            $start_date_formatted = strftime('%d. %B %Y', $start_date);
            $end_date = strtotime($end_date);
            $end_date_formatted = strftime('%d. %B %Y', $end_date);
            $shortEndDate = $start_date_formatted = strftime('%d. %B', $start_date);
            $shortEndDate = utf8_encode($shortEndDate);
            $weekday = strftime('%A', $start_date);

            ?>
            <a href="<?php echo get_page_link(get_page_by_title('termine')->ID) ?>" class="post-title">
                <?php the_title('<h2>', '</h2>'); ?>
            </a>
            <div class="post-meta">
                <div class="post-meta-date">
                    <span><i class="fa fa-calendar-o"></i><?php echo $weekday ?> - <?php echo $shortEndDate ?></span>
                </div>
                <div class="cs-meta-reading-time">
                    <span><i class="fa fa-clock-o"></i><?php echo $start_time  ?></span>
                </div>
                <div class="post-meta-date">
                    <span><i class="fa fa-map-marker"></i><?php echo excerpt(14); ?></span>
                </div>
            </div>
        </div>
    </div>
</article>
