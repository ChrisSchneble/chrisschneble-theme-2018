<?php
/**
 * Blog / Article Page Loop
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('entry-classic clearfix'); ?>>
    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <div class="entry-inner">
            <div class="post-thumb">
                <?php the_post_thumbnail('full'); ?>
            </div>
            <div class="entry-content">
                <div class="entry-date"><?php echo get_the_date(); ?></div>
                <?php the_title('<h2 class="entry-title">', '</h2>'); ?>
                <p class="post-excerpt"><?php echo excerpt(50) ?></p>
            </div>
        </div>
    </a>
</article>
