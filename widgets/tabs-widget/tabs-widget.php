<?php

/*
Widget Name: Tabs Widget
Description: Widget to show Tabs or Accordion
Author: Christian Schneble
Author URI: http://destinationstudios.christianschneble.de
*/

class Tabs_Widget extends SiteOrigin_Widget
{
    function __construct()
    {

        parent::__construct(
            'tabs-widget',
            __('Tabs Widget', 'tabs-widget-domain'),
            array(
                'description' => __('Tabs widget.', 'tabs-widget-domain'),
            ),
            array(),
            array(
                'style' => array(
                    'type' => 'select',
                    'label' => __('Tabs / Accordion', 'tabs-widget-domain'),
                    'default' => 'tabs',
                    'options' => array(
                        'tabs' => __('Tabs', 'tabs-widget-domain'),
                        'accordion' => __('Accordion', 'tabs-widget-domain'),
                    )
                ),
                'styling' => array(
                    'type' => 'select',
                    'label' => __('Styling', 'tabs-widget-domain'),
                    'default' => '',
                    'options' => array(
                        '' => __('Style 1', 'tabs-widget-domain'),
                        'style2' => __('Style 2', 'tabs-widget-domain'),
                    )
                ),
                'contents' => array(
                    'type' => 'repeater',
                    'label' => __('Inhalte', 'tabs-widget-domain'),
                    'item_name' => __('Frame', 'tabs-widget-domain'),
                    'item_label' => array(
                        'selector' => "[id*='frames-url']",
                        'update_event' => 'change',
                        'value_method' => 'val'
                    ),
                    'fields' => array(
                        'headline' => array(
                            'type' => 'text',
                            'label' => __('Überschrift: ', 'dividing-header-widget-text-domain'),
                            'default' => ''
                        ),
                        'iconText' => array(
                            'type' => 'text',
                            'label' => __('Icon: ', 'dividing-header-widget-text-domain'),
                            'default' => ''
                        ),
                        /*'icon' => array(
                            'type' => 'icon',
                            'label' => __('Select an icon', 'widget-form-fields-text-domain'),
                        ),*/
                        'content' => array(
                            'type' => 'tinymce',
                            'label' => __('Inhalt', 'widget-form-fields-text-domain'),
                            'default' => 'An example of a long message.</br>It is even possible to add a few html tags.</br><a href="siteorigin.com" target="_blank"">Links!</a></br><strong>Strong</strong> and <em>emphasized</em> text.',
                            'rows' => 10
                        )
                    ),
                ),
            ),
            plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance)
    {
        return 'tabs-widget-template';
    }

    function get_style_name($instance)
    {
        return 'tabs-widget-style';
    }

}

siteorigin_widget_register('tabs-widget', __FILE__, 'Tabs_Widget');
