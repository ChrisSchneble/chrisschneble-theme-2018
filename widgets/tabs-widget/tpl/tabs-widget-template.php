<?php if (wp_kses_post($instance['style']) == 'tabs') { ?>
    <div class="cs-<?php echo wp_kses_post($instance['style']) ?>">
        <?php $frames = empty($instance['contents']) ? array() : $instance['contents'];
        $i = 1;
        $j = 1;
        if (!empty($frames)) {
            echo '<ul class="nav nav-tabs cs-tabs-filter cs-noselect" role="tablist">';

            foreach ($frames as &$frame) {
                if (!empty($frame['content'])) {
                    echo '<li class="' . ($i > 1 ? '' : 'active') . '"  role="presentation"><a href="#tab-' . $i . '" role="tab" data-toggle="tab"><span class="cs-tabs-icon"><i class="' . $frame['iconText'] . '"></i></span>' . $frame['headline'] . '</a></li>';
                }
                $i++;
            }
            echo '</ul><div class="tab-content">';
            foreach ($frames as &$frame) {
                if (!empty($frame['content'])) {
                    echo '<div role="tabpanel" class="tab-pane fade in ' . ($j > 1 ? '' : 'active') . '" id="tab-' . $j . '">' . $frame['content'] . '</div>';
                }
                $j++;
            }

            echo '</div>';
        } ?>
    </div>
    <?php
} else {
    ?>
    <div class="cs-<?php echo wp_kses_post($instance['style']) ?> panel-group <?php echo wp_kses_post($instance['styling']) ?>" role="tablist" aria-multiselectable="true" id="accordion-1">
        <?php $frames = empty($instance['contents']) ? array() : $instance['contents'];
        $i = 1;
        if (!empty($frames)) {
            foreach ($frames as &$frame) {
                if (!empty($frame['content'])) { ?>
                    <div class="cs-accordion-item panel panel-default">
                        <div class="panel-heading" role="tab" id="heading-<?php echo $i?>">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-<?php echo $i?>" aria-expanded="false" aria-controls="collapse-<?php echo $i?>" class="<?php echo ($i > 1 ? 'collapsed' : '') ?>">
                                    <div class="cs-table">
                                        <div class="cs-table-cell cs-accordion-icon-cell">
                                                    <span class="cs-accordion-icon">
                                                        <i class="open-icon pe-7s-angle-down"></i> <i class="close-icon pe-7s-angle-up"></i> </span></div>
                                        <div class="cs-table-cell cs-accordion-content-cell"><span class="cs-accordion-title"><?php echo $frame['headline'] ?></span></div>
                                    </div>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-<?php echo $i?>" class="panel-collapse collapse <?php echo ($i == 1 ? 'in' : '') ?>" role="tabpanel" aria-labelledby="heading-<?php echo $i?>" aria-expanded="false">
                            <div class="panel-body">
                                <div class="cs-table">
                                    <div class="cs-table-cell-top cs-accordion-content-cell">
                                        <div class="fw-page-builder-content"><?php echo $frame['content'] ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                }
                $i++;
            }
        }
        ?>
    </div>
<?php } ?>




