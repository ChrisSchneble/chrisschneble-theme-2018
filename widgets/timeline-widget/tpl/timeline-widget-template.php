<div class="cs-timeline <?php echo wp_kses_post($instance['cssClass']) ?>">
    <?php
    $isInFuture = wp_kses_post($instance['isInFuture']);


    if ($isInFuture) {
        $args = array(
            'post_type' => 'tf_events',
            'posts_per_page' => '12',
            'post_status' => 'publish',
            'meta_key' => 'tf_events_startdate',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'tf_events_startdate',
                    'value' => date(strtotime('-6 hours')), //value of "order-date"
                    'compare' => '>=', //show events greater than or equal to today
                    'type' => 'CHAR'
                )
            )
        );
    } else {
        $args = array(
            'post_type' => 'tf_events',
            'posts_per_page' => '12',
            'post_status' => 'publish',
            'meta_key' => 'tf_events_startdate',
            'orderby' => 'meta_value_num',
            'order' => 'ASC'
        );
    }
    $the_query = new WP_Query($args); ?>


    <?php if ($the_query->have_posts()) : ?>


        <!-- the loop -->
        <?php while ($the_query->have_posts()) : $the_query->the_post();
            $custom = get_post_custom(get_the_ID());
            $start_date = $custom["tf_events_startdate"][0];

            $end_date = $custom["tf_events_enddate"][0];
            $time_format = get_option('time_format');
            $start_time = date($time_format, $start_date);
            $end_time = date($time_format, $end_date);

            if ($start_date) {
                $start_date = date("d-m-Y", $start_date);
            }

            if ($end_date) {
                $end_date = date("d-m-Y", $end_date);
            }
            setlocale(LC_TIME, 'german', 'deu_deu', 'deu', 'de_DE', 'de');
            $start_date = strtotime($start_date);
            $start_date_formatted = strftime('%d. %B %Y', $start_date);
            $end_date = strtotime($end_date);
            $end_date_formatted = strftime('%d. %B %Y', $end_date);
            $shortEndDate = $start_date_formatted = strftime('%d. %B %Y', $start_date);
            $shortEndDate = utf8_encode($shortEndDate);
            $weekday = strftime('%A', $start_date);
            ?>

            <?php /*echo date('m-d-Y', strtotime('-6 hours')) */ ?>

            <div class="cs-timeline-item">
                <div class="cs-timeline-item-container">
                    <div class="cs-timeline-box cs-timeline-box-left zoomInLeft animated">
                        <div class="cs-table">
                            <div class="cs-timeline-content-container cs-table-cell cs-timeline-content-full"><h2><?php the_title(); ?></h2>
                                <div class="cs-timeline-content">
                                    <div class="event-content"><?php the_content(); ?></div>
                                </div>
                            </div>
                            <div class="cs-timeline-image-container">
                                <div class="cs-ratio">
                                    <div class="cs-ratio-container cs-ratio-container-square">
                                        <div class="cs-ratio-content" style="background-image: url(<?php the_post_thumbnail_url('teaser_thumbnail'); ?>);"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cs-timeline-box-circle"></div>
                        <div class="cs-timeline-box-tale"></div>
                        <div class="cs-timeline-date">
                            <ul class="post-meta">
                                <li class="post-meta-date">
                                    <div class="start-date"><?php echo $shortEndDate ?></div>
                                    <span class="start-day"><?php echo $weekday ?></span>
                                </li>
                                <li class="hour"><?php echo $start_time ?><?php echo($end_time == '00:00' ? '' : ' - ' . $end_time) ?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="cs-timeline-box cs-timeline-box-right zoomInRight animated">
                        <div class="cs-table">
                            <div class="cs-timeline-content-container cs-table-cell cs-timeline-content-full"><h2><?php the_title(); ?></h2>
                                <div class="cs-timeline-content">
                                    <div class="event-content"><?php the_content(); ?></div>
                                </div>
                            </div>
                            <div class="cs-timeline-image-container">
                                <div class="cs-ratio">
                                    <div class="cs-ratio-container cs-ratio-container-square">
                                        <div class="cs-ratio-content" style="background-image: url(<?php the_post_thumbnail_url('teaser_thumbnail'); ?>);"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cs-timeline-box-circle"></div>
                        <div class="cs-timeline-box-tale"></div>
                        <div class="cs-timeline-date">
                            <ul class="post-meta">
                                <li class="post-meta-date">
                                    <div class="start-date"><?php echo $shortEndDate ?></div>
                                    <span class="start-day"><?php echo $weekday ?></span>
                                </li>
                                <li class="hour"><?php echo $start_time ?><?php echo($end_time == '00:00' ? '' : ' - ' . $end_time) ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; else: ?>
        <p>
            <?php _e('Sorry, no events matched your criteria.'); ?>
        </p>
    <?php endif; ?> <?php //wp_reset_query(); ?>

</div>
