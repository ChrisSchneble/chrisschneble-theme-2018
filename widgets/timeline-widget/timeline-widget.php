<?php

/*
Widget Name: Timeline Widget
Description: Widget to show Timeline
Author: Christian Schneble
Author URI: http://destinationstudios.christianschneble.de
*/

class Timeline_Widget extends SiteOrigin_Widget
{
    function __construct()
    {

        parent::__construct(
            'timeline-widget',
            __('Timeline Widget', 'timeline-widget-domain'),
            array(
                'description' => __('Timeline widget.', 'timeline-widget-domain'),
            ),
            array(),
            array(
                'cssClass' => array(
                    'type' => 'text',
                    'label' => __('CSS Klasse: ', 'timeline-widget-domain'),
                    'default' => ''
                ),
                'isInFuture' => array(
                    'type' => 'checkbox',
                    'label' => __('Nur zukünftige', 'timeline-widget-domain'),
                    'default' => false
                ),

            ),
            plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance)
    {
        return 'timeline-widget-template';
    }

    function get_style_name($instance)
    {
        return 'timeline-widget-style';
    }

}

siteorigin_widget_register('timeline-widget', __FILE__, 'Timeline_Widget');
