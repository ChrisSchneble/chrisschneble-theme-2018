<?php

/*
Widget Name: Dividing Header Widget
Description: Widget To Show Headline + Divider'.
Author: Christian Schneble
Author URI: http://destinationstudios.christianschneble.de
*/

class Dividing_Header_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'dividing-header-widget',
			__('Dividing Header Widget', 'dividing-header-widget-text-domain'),
			array(
				'description' => __('Dividing Header widget.', 'dividing-header-widget-text-domain'),
			),
			array(

			),
			array(
				'headline' => array(
					'type' => 'tinymce',
					'label' => __('Headline1: ', 'dividing-header-widget-text-domain'),
					'default' => 'Überschrift 1'
				),
                'subheadline' => array(
					'type' => 'text',
					'label' => __('SubHeadline: ', 'dividing-header-widget-text-domain'),
					'default' => 'Subheadline'
				),
                'divider' => array(
					'type' => 'checkbox',
					'label' => __('Divider: ', 'dividing-header-widget-text-domain'),
					'default' => false
				),
                'divider-small' => array(
					'type' => 'checkbox',
					'label' => __('Small-Gap: ', 'dividing-header-widget-text-domain'),
					'default' => false
				),
                'divider_color' => array(
                    'type' => 'select',
                    'label' => __('Divider Color', 'dividing-header-widget-text-domain'),
                    'default' => 'theme',
                    'options' => array(
                        'theme' => __('Theme Farbe', 'dividing-header-widget-text-domain'),
                        'grey' => __('Grau', 'dividing-header-widget-text-domain')
                    )
                ),
                'style' => array(
                    'type' => 'select',
                    'label' => __('Headline Style', 'dividing-header-widget-text-domain'),
                    'default' => 'center',
                    'options' => array(
                        'center' => __('Mittig', 'dividing-header-widget-text-domain'),
                        'left' => __('Links', 'dividing-header-widget-text-domain'),
                        'full' => __('Volle Breite', 'dividing-header-widget-text-domain'),
                    )
                )
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'dividing-header-widget-template';
	}

	function get_style_name($instance) {
		return 'dividing-header-widget-style';
	}

}

siteorigin_widget_register('dividing-header-widget', __FILE__, 'Dividing_Header_Widget');
