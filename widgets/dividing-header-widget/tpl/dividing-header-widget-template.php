<div class="dividing-headline <?php echo wp_kses_post($instance['style']) ?><?php if (wp_kses_post($instance['divider-small'])) : ?> small-divider<?php endif; ?>">
    <div class="cs-column-wrapper">
        <div class="cs-heading cs-heading-style1">
            <div class="cs-heading-content size-xl">
                <div class="text-center title-text">
                    <?php echo wp_kses_post($instance['headline']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php if (wp_kses_post($instance['subheadline'])) : ?>
        <div class="cs-column-wrapper">
            <div class="cs-heading">
                <div class="cs-heading-content size-custom text-center subline">
                    <?php echo wp_kses_post($instance['subheadline']) ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (wp_kses_post($instance['divider'])) : ?>
        <div class="cs-column-wrapper">
            <div class="cs-divider cs-divider-center cs-divider-content-none">
                <div class="cs-divider-line <?php echo wp_kses_post($instance['divider_color']) ?>"></div>
            </div>
        </div>
    <?php endif; ?>
</div>




