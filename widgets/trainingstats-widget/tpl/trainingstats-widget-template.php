<div class="fw-row trainingstats text-center">

    <div class="trainingStats-menu-bar">
        <a class="cs-button cs-button-medium cs-button-icon-left dropdown-toggle" data-toggle="dropdown" href="#" id="ts-action">
            <span class="cs-button-icon"><i class="pe-7s-angle-down down bounce"></i><i class="pe-7s-angle-up up"></i></span>
            <span class="cs-button-text">Aktivitäten letzte 4 Wochen</span>
        </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="ts-action">
            <li class="monthly"><a href="#" class="active monthly">Aktivitäten letzte 4 Wochen</a></li>
            <li class="current"><a href="#" class="">Aktivitäten im laufenden Jahr</a></li>
        </ul>
    </div>


    <div class="cs-column col-xs-12 col-md-3">
        <div class="cs-column-wrapper">
            <div class="cs-counter <?php echo wp_kses_post($instance['color']) ?> <?php echo wp_kses_post($instance['counterStyle']) ?>">
                <div class="cs-table">
                    <div class="cs-table-cell text-right">
                        <div class="cs-counter-number cs-counter-animate swim">0</div>
                    </div>
                    <div class="cs-table-cell text-left"><span class="cs-counter-title">Swim</span>
                        <div class="cs-counter-subtitle">Kilometer</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cs-column col-xs-12 col-md-3">
        <div class="cs-column-wrapper">
            <div class="cs-counter <?php echo wp_kses_post($instance['color']) ?> <?php echo wp_kses_post($instance['counterStyle']) ?>">
                <div class="cs-table">
                    <div class="cs-table-cell text-right">
                        <div class="cs-counter-number cs-counter-animate bike">0</div>
                    </div>
                    <div class="cs-table-cell text-left"><span class="cs-counter-title">Bike</span>
                        <div class="cs-counter-subtitle">Kilometer</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cs-column col-xs-12 col-md-3">
        <div class="cs-column-wrapper">
            <div class="cs-counter <?php echo wp_kses_post($instance['color']) ?> <?php echo wp_kses_post($instance['counterStyle']) ?>">
                <div class="cs-table">
                    <div class="cs-table-cell text-right">
                        <div class="cs-counter-number cs-counter-animate run">0</div>
                    </div>
                    <div class="cs-table-cell text-left"><span class="cs-counter-title">Run</span>
                        <div class="cs-counter-subtitle">Kilometer</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cs-column col-xs-12 col-md-3">
        <div class="cs-column-wrapper">
            <div class="cs-counter <?php echo wp_kses_post($instance['color']) ?> <?php echo wp_kses_post($instance['counterStyle']) ?>">
                <div class="cs-table">
                    <div class="cs-table-cell text-right">
                        <div class="cs-counter-number cs-counter-animate total">0</div>
                    </div>
                    <div class="cs-table-cell text-left"><span class="cs-counter-title">Gesamt</span>
                        <div class="cs-counter-subtitle">Kilometer</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
