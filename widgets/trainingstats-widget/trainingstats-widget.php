<?php

/*
Widget Name: Training Stats Widget
Description: Widget to show Trainingstats.
Author: Christian Schneble
Author URI: http://destinationstudios.christianschneble.de
*/

class Trainingsstats_Widget extends SiteOrigin_Widget
{
    function __construct()
    {

        parent::__construct(
            'trainingstats-widget',
            __('Trainingsstats Widget', 'trainingstats-widget-text-domain'),
            array(
                'description' => __('Training Stats Widget.', 'trainingstats-widget-text-domain'),
            ),
            array(),
            array(
                'counterStyle' => array(
                    'type' => 'select',
                    'label' => __('Counter Art', 'trainingstats-widget-text-domain'),
                    'default' => 'standard',
                    'options' => array(
                        'standard' => __('Standard', 'trainingstats-widget-text-domain'),
                        'big' => __('Groß', 'trainingstats-widget-text-domain'),
                    )
                ),
                'color' => array(
                    'type' => 'select',
                    'label' => __('Farbe', 'trainingstats-widget-text-domain'),
                    'default' => '',
                    'options' => array(
                        '' => __('standard', 'trainingstats-widget-text-domain'),
                        'white' => __('weiß', 'trainingstats-widget-text-domain'),
                    )
                )

            ),
            plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance)
    {
        return 'trainingstats-widget-template';
    }

    function get_style_name($instance)
    {
        return 'trainingstats-widget-style';
    }

}

siteorigin_widget_register('trainingstats-widget', __FILE__, 'Trainingsstats_Widget');
