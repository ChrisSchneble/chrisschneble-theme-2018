<div id="image-points-<?php echo wp_kses_post($instance['widgetName']); ?>" class="cs-image-points">
    <?php

    $image_attributes = wp_get_attachment_image_src(wp_kses_post($instance['image']), 'full');
    echo '<img src="' . $image_attributes[0] . '" width="' . $image_attributes[1] . '" height="' . $image_attributes[2] . '" alt="' . $image_alt . '"/>'
    ?>
    <?php $points = empty($instance['points']) ? array() : $instance['points'];
    $i = 0;
    if (!empty($points)) {
        foreach ($points as &$point) {
            $right_side = '';
            if ($point['left'] > 65) :
                $right_side .= ' cs-image-point-right';
            else :
                $right_side .= '';
            endif;
            if ($point['left'] > 50) :
                $right_side .= ' cs-image-point-right-mobile';
            else :
                $right_side .= '';
            endif;
            ?>
            <div class="cs-animated zoomIn cs-image-point<?php echo $right_side; ?>" tabindex="0"
                 style="animation-delay: <?php echo intval($i) * 0.25; ?>s; top: <?php echo $point['top']; ?>%; left: <?php echo $point['left']; ?>%;">
                <input id="<?php echo wp_kses_post($instance['widgetName']) . $i ?>" type="checkbox" class="hidden map-point-trigger"><label for="<?php echo wp_kses_post($instance['widgetName']) . $i ?>" class="clickTrigger">
                    <i class="ti-plus"></i>
                </label>
                <div class="cs-image-point-tooltip <?php echo $point['align'] ?>"><?php
                    if (!empty($point['headline'])) {
                        echo "<h4>" . $point['headline'] . "</h4>";
                    }
                    echo $point['content']; ?></div>
            </div>

            <?php $i++;
        }
    } ?>

</div>
