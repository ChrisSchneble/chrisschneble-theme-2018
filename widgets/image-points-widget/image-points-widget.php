<?php

/*
Widget Name: Image_Points Widget
Description: Widget To Show Points on top of Image.
Author: Christian Schneble
Author URI: http://destinationstudios.christianschneble.de
*/

class Image_Points_Widget extends SiteOrigin_Widget
{
    function __construct()
    {

        parent::__construct(
            'image-points-widget',
            __('Image_Points Widget', 'image-points-widget-text-domain'),
            array(
                'description' => __('Image_Points widget.', 'image-points-widget-text-domain'),
            ),
            array(),
            array(
                'widgetName' => array(
                    'type' => 'text',
                    'label' => __('WidgetName', 'image-points-widget-text-domain')
                ),
                'color' => array(
                    'type' => 'select',
                    'label' => __('Punkt-Farbe', 'image-points-widget-text-domain'),
                    'default' => 'theme',
                    'options' => array(
                        'theme' => __('Theme Farbe', 'image-points-widget-text-domain'),
                        'grey' => __('Grau', 'image-points-widget-text-domain')
                    )
                ),
                'image' => array(
                    'type' => 'media',
                    'label' => __('Hintergrundbild', 'image-points-widget-text-domain'),
                    'choose' => __('Choose image', 'image-points-widget-text-domain'),
                    'update' => __('Set image', 'image-points-widget-text-domain'),
                    'library' => 'image',
                    'fallback' => true
                ),
                'points' => array(
                    'type' => 'repeater',
                    'label' => __('Image Point.', 'image-points-widget-text-domain'),
                    'item_name' => __('Point', 'siteorigin-widgets'),
                    'item_label' => array(
                        'selector' => "[id*='repeat_text']",
                        'update_event' => 'change',
                        'value_method' => 'val'
                    ),
                    'fields' => array(
                        'top' => array(
                            'type' => 'text',
                            'label' => __('Top', 'image-points-widget-text-domain')
                        ),
                        'left' => array(
                            'type' => 'text',
                            'label' => __('Left', 'image-points-widget-text-domain')
                        ),
                        'headline' => array(
                            'type' => 'text',
                            'label' => __('Headline', 'image-points-widget-text-domain')
                        ),
                        'content' => array(
                            'type' => 'tinymce',
                            'label' => __('Text', 'image-points-widget-text-domain')
                        ),
                        'align' => array(
                            'type' => 'select',
                            'label' => __('Text Ausrichtung', 'image-gallery-widget-text-domain'),
                            'default' => 'text-center',
                            'options' => array(
                                'text-center' => __('Mittig', 'image-gallery-widget-text-domain'),
                                'text-left' => __('Links', 'image-gallery-widget-text-domain'),
                                'text-right' => __('Rechts', 'image-gallery-widget-text-domain'),
                            )
                        ),
                    )
                )
            ),
            plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance)
    {
        return 'image-points-widget-template';
    }

    function get_style_name($instance)
    {
        return 'image-points-widget-style';
    }

}

siteorigin_widget_register('image-points-widget', __FILE__, 'Image_Points_Widget');
