<?php

/*
Widget Name: Social Image Widget
Description: Footer widget for image and Social Icons.
Author: Christian Schneble
Author URI: http://destinationstudios.christianschneble.de
*/

class Social_Image_Widget extends SiteOrigin_Widget
{
    function __construct()
    {

        parent::__construct(
            'social-image-widget',
            __('Social Image Widget', 'social-image-widget-text-domain'),
            array(
                'description' => __('Social Image widget.', 'social-image-widget-text-domain'),
            ),
            array(),
            array(
                'text' => array(
                    'type' => 'text',
                    'label' => __('Widget Text: ', 'dividing-header-widget-text-domain'),
                    'default' => ''
                ),
                'facebook' => array(
                    'type' => 'link',
                    'label' => __('Facebook Url: ', 'dividing-header-widget-text-domain'),
                    'default' => ''
                ),
                'twitter' => array(
                    'type' => 'link',
                    'label' => __('Twitter Url: ', 'dividing-header-widget-text-domain'),
                    'default' => ''
                ),
                'instagram' => array(
                    'type' => 'link',
                    'label' => __('Instagram Url: ', 'dividing-header-widget-text-domain'),
                    'default' => ''
                ),
                'youtube' => array(
                    'type' => 'link',
                    'label' => __('YouTube Url: ', 'dividing-header-widget-text-domain'),
                    'default' => ''
                ),
                'vimeo' => array(
                    'type' => 'link',
                    'label' => __('Vimeo Url: ', 'dividing-header-widget-text-domain'),
                    'default' => ''
                ),
                'image' => array(
                    'type' => 'media',
                    'label' => __('Bild', 'widget-form-fields-text-domain'),
                    'choose' => __('Bild wählen', 'widget-form-fields-text-domain'),
                    'update' => __('Set image', 'widget-form-fields-text-domain'),
                    'library' => 'image',
                    'fallback' => true
                )


            ),
            plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance)
    {
        return 'social-image-widget-template';
    }

    function get_style_name($instance)
    {
        return 'social-image-widget-style';
    }

}

siteorigin_widget_register('social-image-widget', __FILE__, 'Social_Image_Widget');
