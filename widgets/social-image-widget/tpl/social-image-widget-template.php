
<?php echo wp_get_attachment_image( wp_kses_post($instance['image']) ,'full', ['loading' => 'false']);  ?>
<?php if (wp_kses_post($instance['text'])) : ?>
    <p class="cs-image-widgets-description">
        <?php echo wp_kses_post($instance['text']) ?>
    </p>
<?php endif; ?>

<p class="cs-image-widgets-description cs-image-widgets-social">

    <?php if (wp_kses_post($instance['facebook'])) : ?>
        <a href="<?php echo wp_kses_post($instance['facebook']) ?>" target="_blank" class="social-media-facebook">
            <i class="icon-social-facebook"></i>
        </a>
    <?php endif; ?>
    <?php if (wp_kses_post($instance['twitter'])) : ?>
        <a href="<?php echo wp_kses_post($instance['twitter']) ?>" target="_blank" class="social-media-twitter">
            <i class="icon-social-twitter"></i>
        </a>
    <?php endif; ?>
    <?php if (wp_kses_post($instance['instagram'])) : ?>
            <a href="<?php echo wp_kses_post($instance['instagram']) ?>" target="_blank" class="social-media-instagram">
                <i class="icon-social-instagram"></i>
            </a>
        <?php endif; ?>
    <?php if (wp_kses_post($instance['youtube'])) : ?>
                <a href="<?php echo wp_kses_post($instance['youtube']) ?>" target="_blank" class="social-media-youtube">
                    <i class="icon-social-youtube"></i>
                </a>
            <?php endif; ?>
    <?php if (wp_kses_post($instance['vimeo'])) : ?>
        <a href="<?php echo wp_kses_post($instance['vimeo']) ?>" target="_blank" class="social-media-vimeo">
            <i class="icon-screen-smartphone"></i>
        </a>
    <?php endif; ?>

</p>



