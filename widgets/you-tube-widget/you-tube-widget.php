<?php

/*
Widget Name: You Tube Widget
Description: Widget to show You Tube Items of User
Author: Christian Schneble
Author URI: http://destinationstudios.christianschneble.de
*/

class You_Tube_Widget extends SiteOrigin_Widget
{
    function __construct()
    {

        parent::__construct(
            'you-tube-widget',
            __('You Tube Widget', 'you-tube-widget-domain'),
            array(
                'description' => __('You Tube widget.', 'you-tube-widget-domain'),
            ),
            array(),
            array(
                'id' => array(
                    'type' => 'text',
                    'label' => __('You Tube ID: ', 'you-tube-widget-domain'),
                    'default' => ''
                ),
                'maxItems' => array(
                    'type' => 'number',
                    'label' => __('Max. Anzahl: ', 'you-tube-widget-domain'),
                    'default' => 10
                ),


            ),
            plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance)
    {
        return 'you-tube-widget-template';
    }

    function get_style_name($instance)
    {
        return 'you-tube-widget-style';
    }

}

siteorigin_widget_register('you-tube-widget', __FILE__, 'You_Tube_Widget');
