<?php

/*
Widget Name: Frame Widget
Description: Widget To Show an image with a Textframe on it.
Author: Christian Schneble
Author URI: http://destinationstudios.christianschneble.de
*/

class Frame_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'frame-widget',
			__('Frame Widget', 'frame-widget-text-domain'),
			array(
				'description' => __('Frame widget.', 'frame-widget-text-domain'),
			),
			array(

			),
			array(
				'text1' => array(
					'type' => 'text',
					'label' => __('Text1: ', 'frame-widget-text-domain'),
					'default' => 'Text 1'
				),
                'text2' => array(
					'type' => 'text',
					'label' => __('Text2: ', 'frame-widget-text-domain'),
					'default' => 'Text 2'
				),
                'opacity' => array(
                    'type' => 'slider',
                    'label' => __('Opacity only 10ths', 'widget-form-fields-text-domain'),
                    'default' => 100,
                    'min' => 0,
                    'max' => 100,
                    'integer' => true
                ),
                'image' => array(
                    'type' => 'media',
                    'label' => __('Choose a media thing', 'widget-form-fields-text-domain'),
                    'choose' => __('Choose image', 'widget-form-fields-text-domain'),
                    'update' => __('Set image', 'widget-form-fields-text-domain'),
                    'library' => 'image',
                    'fallback' => true
                )
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'frame-widget-template';
	}

	function get_style_name($instance) {
		return 'frame-widget-style';
	}

}

siteorigin_widget_register('frame-widget', __FILE__, 'Frame_Widget');
