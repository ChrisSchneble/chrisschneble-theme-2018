<div class="cs-text-with-frame opacity-<?php echo wp_kses_post($instance['opacity']) ?>">
    <div class="cs-text-with-frame-container">
        <p>
            <span class="small-text"><?php echo wp_kses_post($instance['text1']) ?></span>
        </p>
        <span class="big-text"><?php echo wp_kses_post($instance['text2']) ?></span></div>
    <?php

    $image_attributes = wp_get_attachment_image_src(wp_kses_post($instance['image']), 'full');
    echo '<img src="' . $image_attributes[0] . '" width="' . $image_attributes[1] . '" height="' . $image_attributes[2] . '" alt="' . $image_alt .'"/>'
    ?>
</div>

