<h3 class="widget-title">Kontakt</h3>
<div class="cs-contacts-widget-item"><i class="icon-map"></i> <?php echo wp_kses_post($instance['address']) ?></div>
<div class="cs-contacts-widget-item"><i class="icon-phone"></i> <?php echo wp_kses_post($instance['phone']) ?></div>
<div class="cs-contacts-widget-item"><i class="icon-envelope"></i>
    <a href="<?php echo get_page_link(get_page_by_title(wp_kses_post($instance['message']))->ID )?>">Nachricht Senden</a>
</div>
<?php if (wp_kses_post($instance['additional'])) : ?>
    <div class="cs-contacts-widget-item"><i class="icon-clock"></i> <?php echo wp_kses_post($instance['additional']) ?></div>
<?php endif; ?>







