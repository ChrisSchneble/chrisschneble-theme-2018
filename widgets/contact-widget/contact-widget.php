<?php

/*
Widget Name: Contact Widget
Description: Displays Contact Infos.
Author: Christian Schneble
Author URI: http://destinationstudios.christianschneble.de
*/

class Contact_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'contact-widget',
			__('Contact Widget', 'contact-widget-text-domain'),
			array(
				'description' => __('Contact widget.', 'contact-widget-text-domain'),
			),
			array(

			),
			array(
				'address' => array(
					'type' => 'text',
					'label' => __('Adresse: ', 'contact-widget-text-domain'),
					'default' => 'Adresse'
				),
                'phone' => array(
					'type' => 'text',
					'label' => __('Telefonnumer: ', 'contact-widget-text-domain'),
					'default' => ''
				),
                'message' => array(
                    'type' => 'text',
                    'label' => __('Nachricht senden Seite ', 'contact-widget-text-domain'),
                    'default' => ''
                ),
                'additional' => array(
                    'type' => 'text',
                    'label' => __('Weiterer Inhalt: ', 'contact-widget-text-domain'),
                    'default' => ''
                ),

			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'contact-widget-template';
	}

	function get_style_name($instance) {
		return 'contact-widget-style';
	}

}

siteorigin_widget_register('contact-widget', __FILE__, 'Contact_Widget');
