<?php

/*
Widget Name: Image Gallery Widget
Description: Widget to show an Image Gallery.
Author: Christian Schneble
Author URI: http://destinationstudios.christianschneble.de
*/

class Image_Gallery_Widget extends SiteOrigin_Widget
{
    function __construct()
    {

        parent::__construct(
            'image-gallery-widget',
            __('Image Gallery Widget', 'image-gallery-widget-text-domain'),
            array(
                'description' => __('Image Gallery widget.', 'image-gallery-widget-text-domain'),
            ),
            array(),
            array(
                'insta' => array(
                    'type' => 'text',
                    'label' => __('Instagram User', 'widget-form-fields-text-domain'),
                    'default' => ''
                ),
                'imagecount' => array(
                    'type' => 'number',
                    'label' => __('Anzahl Bilder', 'image-gallery-widget-text-domain'),
                    'default' => '8'
                ),
                'columns' => array(
                    'type' => 'number',
                    'label' => __('Anzahl Zeilen', 'image-gallery-widget-text-domain'),
                    'default' => '4'
                ),
                'galleryStyle' => array(
                    'type' => 'select',
                    'label' => __('Gallery Art', 'image-gallery-widget-text-domain'),
                    'default' => 'masonry',
                    'options' => array(
                        'carousel' => __('Carousel', 'image-gallery-widget-text-domain'),
                        'carousel skew' => __('Carousel Skew', 'image-gallery-widget-text-domain'),
                        'masonry' => __('Mansory', 'image-gallery-widget-text-domain'),
                    )
                ),
                'images' => array(
                    'type' => 'repeater',
                    'label' => __('Bilder', 'image-gallery-widget-text-domain'),
                    'item_name' => __('Frame', 'image-gallery-widget-text-domain'),
                    'item_label' => array(
                        'selector' => "[id*='frames-url']",
                        'update_event' => 'change',
                        'value_method' => 'val'
                    ),
                    'fields' => array(
                        'image' => array(
                            'type' => 'media',
                            'library' => 'image',
                            'label' => __('Bild', 'image-gallery-widget-text-domain'),
                            'fallback' => true,
                        )
                    ),
                ),
            ),
            plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance)
    {
        return 'image-gallery-widget-template';
    }

    function get_style_name($instance)
    {
        return 'image-gallery-widget-style';
    }

}

siteorigin_widget_register('image-gallery-widget', __FILE__, 'Image_Gallery_Widget');
