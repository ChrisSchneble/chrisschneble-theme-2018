<?php if (wp_kses_post($instance['galleryStyle']) == 'carousel') {
    $cssClass = 'carousel cs-image-gallery';
} elseif (wp_kses_post($instance['galleryStyle']) == 'carousel skew') {
    $cssClass = 'carousel cs-image-gallery skew';
} else {
    $cssClass = 'masonry';
}
?>

<div class="wrapper galleryWrapper">
    <?php if (!empty(wp_kses_post($instance['insta']))) { ?>
        <div id="instafeed-gallery-feed" class="masonry instagram" data-user-id="<?php echo wp_kses_post($instance['insta'])?>">
                <div id="progressbar"><span id="loading"></span>
                    <div id="load">lade Instagram Daten</div>
                </div>
        </div>
        <div class="cs-button-container cs-button-style-2 btn-group insta hidden">
            <a id="btn-instafeed-load" class="cs-button cs-button-medium cs-button-icon-right">
                <span class="cs-button-text">Mehr Laden</span>
            </a>
        </div>
    <?php } else { ?>
        <div class="<?php echo $cssClass ?>" data-columns="<?php echo wp_kses_post($instance['columns']) ?>">
            <?php $frames = empty($instance['images']) ? array() : $instance['images'];
            $i = 1;
            if (!empty($frames)) {
                foreach ($frames as &$frame) {
                    if (!empty($frame['image'])) {
                        $attachment = $frame['image'];
                        $url = wp_get_attachment_url($attachment, 'full');
                        $img = wp_get_attachment_url($attachment);
                        $image_alt = get_post_meta( $attachment, '_wp_attachment_image_alt', true);
                        if (wp_kses_post($instance['galleryStyle']) == 'carousel skew') {
                            $image_attributes = wp_get_attachment_image_src($attachment, 'teaser_small');
                        } else {
                            $image_attributes = wp_get_attachment_image_src($attachment);
                        }
                        echo '<div class="cover"><div class="item"><a class="brick " data-rel="lightcase:slideshow" href="' . $url . '"><img src="' . $image_attributes[0] . '" width="' . $image_attributes[1] . '" height="' . $image_attributes[2] . '" alt="' . $image_alt .'"/></a></div></div>';
                        if ($i == wp_kses_post($instance['imagecount'])) break;
                        $i++;
                    }
                }
            } ?>
        </div>
    <?php } ?>
</div>



