<?php
/**
 * The template for displaying 404 pages (Not Found).
 */

get_header(); ?>

<div class="container entry-content">
    <div id="cs-404" class="cs-404 cs-table">
        <div class="cs-table-cell">
            <span class="cs-404-title">404</span>
            <div>
                <h3>Hoppla diese Seite konnte nicht gefunden werden</h3>
                <p>Es scheint als wären sie vom Kurs abgekommen.</p>
                <a href="<?php echo get_bloginfo('url') ?>" class="cs-404-button">
                    <i class="fa fa-refresh"></i>
                    Startseite
                </a>
            </div>
        </div>
    </div>

    <div id="cs-404-mobile" class="cs-404">
        <div class="cs-table-cell">
            <span class="cs-404-title cs-404-mobile-title">404</span>
            <div>
                <h3>Hoppla diese Seite konnte nicht gefunden werden</h3>
                <p>Es scheint als wären sie vom Kurs abgekommen.</p>
                <a href="<?php echo get_bloginfo('url') ?>" class="cs-404-button">
                    <i class="fa fa-refresh"></i>
                    Startseite
                </a>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
