<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since cstheme 1.0
 */


get_header(); ?>


<?php if (is_front_page()) :
    echo '<div id="content" class="page-content">';
    while (have_posts()) : the_post();
        the_content();
    endwhile;
    echo "</div>";
else :
    get_template_part('template-parts/content', 'page');
endif;
?>
<?php get_footer(); ?>
